# openEHR data modelling: histopathology  #
------

This repository contains a set of **openEHR models** developed to represent clinical **data sets derived from the analysis of hystopatological slides**, using the openEHR formalism. This is a proof-of-concept to evaluate the feasibility of using openEHR formalism to model such datasets. 

The main use case considered here is prostatic cancer, as part of the research activities within the ProMort Project[^1]. 

[^1]: https://doi.org/10.1093/aje/kwz026
